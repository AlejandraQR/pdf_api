console.log("Aqui funcionando con nodemon");

var movimientosJSON =require('./movimientosv2.json');
var usuariosJSON = require('./usuariosv1.json');
 fs = require('fs');
//require instancia objetos o archivos libreria
var express = require('express');
//Para que express pueda utilizar la libreria body parser
var bodyparser = require('body-parser');
var jsonQuery = require('json-query');
var requestJson = require('request-json');

//Indicas a app que usuara la libreria express
var app = express();
//le indicas a app que es una instancia de express y que utilizaras el metodo bodyparser
app.use(bodyparser.json());

app.get('/', function (req, res)
{
 res.send("Hola API");
});

app.get('/v1/movimientos', function(req, res){

  res.sendfile("movimientosv1.json");
});

app.get('/v2/movimientos', function(req, res){
  res.send(movimientosJSON);
});

app.get('/v1/usuarios', function(req, res){
  res.send(usuariosJSON);
});

app.get('/v2/movimientos/:id' , function(req, res){
  //  console.log(req)
    console.log(movimientosJSON);

  //  var id=req.params.id
    console.log(req.params.id);
    //res.send("Hemos recibido su peticion de consulta de movimiento #: " + id)
    res.send(movimientosJSON[req.params.id-1]);
});


app.post('/v2/movimientos', function (req, res)
{

//if(req.headers["authorization"]== "100"){
  //console.log(req);
  //dar in ID al registro
  var nuevo = req.body;
  nuevo.id = movimientosJSON.length + 1;
  movimientosJSON.push(nuevo);
    //Da de alta del Json en el Archivo
  //Respuesta del alta
  res.send("Movimiento dado de alta");
});
/*else{
  res.send("no esta autorizado")
}*/
//});

//agregando un PUT equipo
app.put('/v2/movimientos/:id', function (req, res)
{
  var viejo = movimientosJSON[req.params.id-1];
  var nuevo = req.body;
  nuevo.id =req.params.id;
  movimientosJSON[req.params.id-1]= nuevo;
  res.send("Movimiento actualizado");
});

/*Modo instructor
app.put('/v2/movimientos/:id', function (req, res)
{
  var cambios = req.body;
  movimientosJSON[req.params.id-1]= cambios;
  res.send("Movimiento actualizado");
});*/


//Actualiza solo los valores que indicas
app.put('/v2/movimientos/:id', function (req, res)
{
  var cambios = req.body;
  var actual = movimientosJSON[req.params.id-1];
  if(cambios.importe != undefined)
  {
    actual.importe = cambios.importe;
  }
  if (cambios.ciudad != undefined)
  {
    actual.ciudad = cambios.ciudad;
  }
  res.send("Importe y ciudad actualizados");
});

app.delete('/v2/movimientos/:id', function (req, res)
{
  var actual = movimientosJSON[req.params.id -1];
  movimientosJSON.push({
    "id": movimientosJSON.length + 1,
    "ciudad": actual.ciudad,
    "importe": actual.importe *(-1),
    "concepto": "negativo del registro con id: "+ req.params.id
  });
  res.send("Movimiento anulado");
});

//GET de prueba

app.get('/v2/movimientosq', function (req, res)
{
  console.log(req.query);
  res.send("Recibido");
});



/**********************************************************Usuarios**************************************/



app.get('/v1/usuarios/:id' , function(req, res){
    console.log(usuariosJSON);
    console.log(req.params.id);
    res.send(usuariosJSON[req.params.id-1]);
});

app.post('/v1/usuarios' , function(req, res){
    var usuario = req.body;

  for (var i = 0; i < usuariosJSON.length ; i++) {
    console.log(usuario)
    var encontrado = null
    if(usuario.correo== usuariosJSON[i].email &&  usuario.password == usuariosJSON[i].Password){
      encontrado = true
      usuariosJSON[i].estado = "OnLine";
      res.send('{"status":{"code": 200, "description": "Login Exitoso"}')
    }
  }
  if (!encontrado) {
      res.send('{"status":{"code": 401, "description": "Ususario no encontrado"}')
    }
});

app.post('/v1/usuarios/:id', function(req,res)
{
  var viejo = req.body
  var nuevo = usuariosJSON[req.params.id-1];
  nuevo.estado = "OffLine"
  usuariosJSON.push(nuevo)      //actualiza sesion como false
  res.send('{"status":{"code": 200, "description": "Logout Exitoso"}')
} )
/**********************FIN USUARIOS EJERCICIOS************************************************/
/*************************************INICIO DE CÒDIGO DE INSTRUCTOR*************************+/
//capturador de peticiones post login
/*app.post("/v2/usuarios/login", function(req,res)
{
  var pass=req.headers["pass"];
  var email=req.headers["email"];

  var resultado=jsonQuery("[email="+email+"]",{data:usuariosJSON});
  if(resultado.value!=null)
  {
    if(usuariosJSON[resultado.value.id-1].pass==resultado.value.pass)
    {
      usuariosJSON[resultado.value.id-1].estado="logeado";
      res.send("{'login':'ok'}");
    }else
    {
      res.send('{"login":"ok"}');
    }
  }else
  {
    res.send('{"login":"error"}');
  }
  */
/*******************FIN DE CÒDIGO DE INSTRUCTOR***********************************/


/**********************Version 3 de la API conectada a MLAB******************************/


/*var urlMlabRaiz = "https://api.mlab.com/api/1/databases/techumx/collections";
var apiKey = "apiKey=zvt5QYfGDMf1urNWSF25ynSwzJo1x-gH";
var clienteMlab = requestJson.createClient(urlMlabRaiz + "?" + apiKey);
app.get ('/v3', function (req, res){
    var collectionesUsuario = [];
    if (!err) {
        for (var i = 0; i < body.length; i++) {
            if(body[i] != "system.indexes"){
                collectionesUsuario.push(body[i]);
            }
        }
          res.send(collectionesUsuario);
      }
      else {
        res.send(err);
      }
});*/

var urlMlabRaiz="https://api.mlab.com/api/1/databases/techumx/collections";
var apiKey="apiKey=zvt5QYfGDMf1urNWSF25ynSwzJo1x-gH";
var clienteMlab=requestJson.createClient(urlMlabRaiz + "?" +apiKey);

app.get("/v3",function(req,res)
{
  clienteMlab.get("", function(err,resM,body)
  {
    var coleccionesUsuario=[];
    if(!err)
    {
      for (var i = 0; i < body.length; i++)
      {
        if(body[i]!="system.indexes")
        {
          coleccionesUsuario.push({"recurso":body[i], "url":"/v3/"+body[i]});

        }
      }
      res.send(coleccionesUsuario);
    }else
    {
      res.send(err);
    }
  });
})

app.get('/v3/usuarios', function(req, res){
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey);
  clienteMlab.get('', function(err,resM,body)
    {
      res.send(body);
    })
})

app.post('/v3/usuarios', function(req, res){
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey);
  clienteMlab.post('', req.body, function(err,resM,body)
    {
      res.send(body);
    })
});

app.get('/v3/usuarios/:id', function(req, res){
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios");
    clienteMlab.get('?q={"id": ' + req.params.id +'}&'+apiKey,
    function(err,resM,body){
      res.send(body)
  })
});

app.put('/v3/usuarios/:id', function(req,res)
{
clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios")
var cambio = '{"$set":'+ JSON.stringify(req.body) + '}'
console.log(req.body)
clienteMlab.put('?q={"id":' + req.params.id + '}&' + apiKey, JSON.parse(cambio), function(err, resM, body) {
  res.send(body)
})
})

/***********************V3 de movimientos***************************/
app.get('/v3/movimientos', function(req, res){
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/movimientos?" + apiKey);
  clienteMlab.get('', function(err,resM,body)
    {
      res.send(body);
    })
})

app.get('/v3/movimientos/:id', function(req, res){
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/movimientos");
    clienteMlab.get('?q={"id": ' + req.params.id +'}&'+apiKey,
    function(err,resM,body){
      res.send(body)
  })
});

/**********************************************V4 Archivo PDF*****************************/
app.get('/v4', function(req, res){

    fs.readFile(  "./ElAlquimista.pdf" , function (err,data){
        res.contentType("application/pdf");
        res.send(data);
    });
});

app.listen(3000);
console.log("Escuchando en el puerto 3000");



/********************************************cuentas y usuario**************************************/

/*Trae todas las cuentas*/

app.get('/v3/cuentas', function(req, res){
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + apiKey);
  clienteMlab.get('', function(err,resM,body)
    {
      res.send(body);
    })
})
/****trea los datos por cuenta***/
app.get('/v3/movimientoscuentas/:id', function(req, res){
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas");
  console.log(urlMlabRaiz + "/cuentas");
  console.log('?q={"idcuenta":"' + req.params.id +'"}&'+apiKey);
  clienteMlab.get('?q={"idcuenta":"' + req.params.id +'"}&'+apiKey,
  function(err,resM,body){
    res.send(body)
  })
})

/****trea los datos por cuenta***/
app.get('/v3/cuentasusuarios/:id', function(req, res){
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas");
  console.log(urlMlabRaiz + "/cuentas");
  console.log('?q={"idusuario":"' + req.params.id +'"}&'+apiKey);
  clienteMlab.get('?q={"idusuario":"' + req.params.id +'"}&'+apiKey,
  function(err,resM,body){
    res.send(body)
  })
})
