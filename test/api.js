var mocha = require ('mocha')
var chai = require ('chai')
var chaiHttp = require('chai-http')
var should = chai.should()
var server = require('../server') //agregamos el archivo donde esta el código de mi archivo server de la carpeta API

chai.use(chaiHttp)// configurar chai con el módulo https
describe('test de conectividad', ()=>{
  it('Google funciona', (done)=>
  {
    chai.request('http://www.google.com.mx').get('/').end((err,res)=>
    {
    //  console.log(res);
    res.should.have.status(200)
    done()
    })
  })
})

describe('Tests de API usuarios', ()=>
{
  it('Raíz de la API funciona', (done)=>
    {
    chai.request('http://localhost:3000').get('/v3').end((err,res)=>
    {
    //  console.log(res);
    res.should.have.status(200)
    done()
    })
  })
});

  describe('Raiz de la API funciona', ()=>{
    it('Raíz de la API funciona', (done)=>
    {
      chai.request('http://localhost:3000').get('/v3').end((err,res)=>
      {
      //  console.log(res);
      res.should.have.status(200)
      res.body.should.be.a('array')
      done()
      })
    })
})

describe('Raíz de la API devuelve 2 colecciones', ()=>
  {
    it('Raíz de la API devuelve 2 colecciones', (done)=>
      {
        chai.request('http://localhost:3000').get('/v3').end((err,res)=>
          {
            //  console.log(res);
            res.should.have.status(200)
            res.body.should.be.a('array')
            res.body.length.should.be.eql(2)
            done()
          })
        })
  })

  describe('Raíz de la API devuelve 2 colecciones', ()=>
    {
      it('Raíz de la API devuelve 2 colecciones', (done)=>
        {
          chai.request('http://localhost:3000').get('/v3').end((err,res)=>
            {
              //  console.log(res);
              res.should.have.status(200)
              res.body.should.be.a('array')
              res.body.length.should.be.eql(2)
              for (var i = 0; i < res.body.length; i++) {
              res.body[i].should.have.property("recurso")
              res.body[i].should.have.property("url")
              }
              done()
            })
          })
    });

    //movimientos
    describe('Tests de API movimientos', ()=>
    {
      it('Raíz de la API movimientos funciona', (done)=>
        {
          chai.request('http://localhost:3000').get('/v3/movimientos').end((err,res)=>
            {
              //  console.log(res);
              res.should.have.status(200)
              console.log(res.body.length);
              done()
            })
        })
    });


/*var tamano =0;
    describe('Tests de API movimientos por ID', ()=>
    {
      it('contando los id funciona', (done)=>
        {
          chai.request('http://localhost:3000').get('/v3/movimientos/').end((err,res)=>
            {
             tamano = res.body.length;
             console.log(tamano);
              //done()
            })

            for (var i = 0; i <= tamano; i++)
            {
              chai.request('http://localhost:3000').get('/v3/movimientos/'+ (i+1)).end((err,res)=>
                {
                  res.should.have.status(200)
                  res.body.should.have.property("id").eql(i+1)
                  done();
                })
            }
        })
    });*/
